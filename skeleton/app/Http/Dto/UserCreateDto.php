<?php

namespace App\Http\Dto;

class UserCreateDto
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

}