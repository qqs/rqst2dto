<?php

namespace App\Http\Dto\SomeDir;

class SomeDto
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $author;

    /**
     * @var string
     */
    public $description;

    /**
     * @var \Carbon\Carbon
     */
    public $published_date;

    /**
     * @var \Illuminate\Http\UploadedFile|null
     */
    public $image;

}