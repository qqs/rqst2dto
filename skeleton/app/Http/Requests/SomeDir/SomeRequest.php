<?php

namespace App\Http\Requests\SomeDir;

use Illuminate\Foundation\Http\FormRequest;

class SomeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.required' => 'Введите название книги',
            'author.required' => 'Введите автора книги',
            'description.required' => 'Введите описание книги',
            'published_date.required' => 'Введите дату публикации книги',
            'image.image' => 'Файл должен быть изображением',
            'image.mimes' => 'Файл должен быть одним из форматов: jpeg, png, jpg, gif',
            'image.max' => 'Максимальный размер файла: 2048 КБ',
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'author' => 'required|max:255',
            'description' => 'required|max:1000',
            'published_date' => 'required|date',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
}