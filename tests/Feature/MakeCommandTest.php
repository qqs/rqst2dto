<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakeCommandTest extends TestCase
{
    const REQUESTS_NAMESPACE = 'App\\';
    const TEMPLATE_CREATED = "DTO class %s has been created.";
    use RefreshDatabase;

    /**
     * @test
     * @testdox с флагом --all работает нормально
     */
    public function allFlagWorkProperly()
    {
        $this->artisan('make:dto', [
            '--all' => true,
        ]);

        $this->assertFileExists(app_path('Http\\Dto\\BookCreateDto.php'));
        $this->assertFileExists(app_path('Http\\Dto\\UserCreateDto.php'));
    }

    /**
     * @test
     */
    public function makeDTOWithInvalidRequestClass()
    {
        $this->artisan('make:dto', [
            'name' => 'InvalidRequest',
        ])->expectsOutput('Request class InvalidRequest does not exist.');
    }

    /**
     * @test
     */
    public function makeDTOWithValidRequestClass()
    {
        $this->artisan('make:dto', [
            'name' => 'SomeRequest',
        ]);

        $this->assertFileExists(app_path('Http\\Dto\\SomeDir\\SomeDto.php'));
    }

}