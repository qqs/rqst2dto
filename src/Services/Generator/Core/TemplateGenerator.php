<?php

namespace Qqs\Rqst2Dto\Services\Generator\Core;

class TemplateGenerator
{
    public function generate($className, $namespace, $properties)
    {
        $content = "<?php\n\n";
        $content .= "namespace $namespace;\n\n";
        $content .= "class $className\n{\n";

        foreach ($properties as $name => $type) {
            $content .= "    /**\n";
            $content .= "     * @var $type\n";
            $content .= "     */\n";
            $content .= "    public \$$name;\n\n";
        }

        $content .= "}";
        return $content;
    }
}