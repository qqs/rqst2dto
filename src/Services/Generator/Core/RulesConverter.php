<?php

namespace Qqs\Rqst2Dto\Services\Generator\Core;

use Illuminate\Support\Str;

class RulesConverter
{
    /**
     * Get type from rules.
     *
     * @param array $rules
     * @return string
     */
    private static function getTypeFromRules(array $rules): string
    {
        // Check if array
        if (in_array('array', $rules)) {
            return 'array';
        }

        // Check if boolean
        if (in_array('boolean', $rules)) {
            return 'bool';
        }

        // Check if integer
        if (in_array('integer', $rules)) {
            return 'int';
        }

        // Check if numeric
        if (in_array('numeric', $rules)) {
            return 'float';
        }

        // Check if date
        if (in_array('date', $rules)) {
            return '\Carbon\Carbon';
        }

        // Check if image
        if (in_array('image', $rules)) {
            return '\Illuminate\Http\UploadedFile';
        }

        // Check if in array
        $inArray = collect($rules)->first(function ($rule) {
            return Str::startsWith($rule, 'in:');
        });

        if ($inArray) {
            $values = explode(',', Str::after($inArray, 'in:'));
            $values = collect($values)->map(function ($value) {
                return trim($value, ' "');
            });
            $values = $values->implode('|');

            return "string|in:{$values}";
        }

        // Check if dimensions
        $dimensions = collect($rules)->first(function ($rule) {
            return $rule instanceof \Illuminate\Validation\Rules\Dimensions;
        });

        if ($dimensions) {
            return '\Illuminate\Http\UploadedFile';
        }

        return 'string';
    }

    public function convert(array $rules): array
    {
        $properties = [];

        foreach ($rules as $field => $rulesString) {
            $fieldRules = is_array($rulesString) ? $rulesString : explode('|', $rulesString);

            $type = $this->getTypeFromRules($fieldRules);

            // Check if required
            if (! in_array('required', $fieldRules) && ! Str::endsWith($type, '?')) {
                $type .= '|null';
            }

            $properties[$field] = $type;
        }

        return $properties;
    }
}