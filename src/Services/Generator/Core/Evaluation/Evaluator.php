<?php

namespace Qqs\Rqst2Dto\Services\Generator\Core\Evaluation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use function class_basename;
use function str_replace;

class Evaluator
{
    const DEFAULT_NAMESPACE_REQUESTS = 'App\Http\Requests';
    const DEFAULT_NAMESPACE_DTO = 'App\\Http\\Dto';
    const DEFAULT_DIRECTORY_DTO = 'app\\Http\\Dto\\';

    public function evaluate(FormRequest $formRequest): Result
    {
        $resultBaseClassName = str_replace('Request', 'Dto', class_basename($formRequest));
        $namespaceDtoWithClassName = get_class($formRequest);
        // Извлекаем неймспейс класса FormRequest
        $namespaceDto = Str::beforeLast($namespaceDtoWithClassName, '\\');
        // Извлекаем нейм класса FormRequest
        // Определяем префикс на основе неймспейса FormRequest
        if (
            $namespaceDto == self::DEFAULT_NAMESPACE_REQUESTS
            || ($namespaceDto === 'App')
            || Str::endsWith($namespaceDto, 'Http')
        ) {
            $resultNamespace = self::DEFAULT_NAMESPACE_DTO;
        } else {
            $namespaceDtoWithoutDefault = Str::afterLast($namespaceDto, self::DEFAULT_NAMESPACE_REQUESTS);
            $resultNamespace = self::DEFAULT_NAMESPACE_DTO . $namespaceDtoWithoutDefault;
        }
//        $resultNamespace = rtrim($resultNamespace, '\\');
        $relativePath = str_replace('App', 'app', $resultNamespace);
        $resultDirectory = base_path($relativePath);
        $filename = "$resultBaseClassName.php";
        $resultFilePath = "$resultDirectory/$filename";

        $data = new Result();
        $data->baseClassName = $resultBaseClassName;
        $data->filePath = $resultFilePath;
        $data->namespace = $resultNamespace;
        $data->directory = $resultDirectory;
        return $data;
    }

}