<?php

namespace Qqs\Rqst2Dto\Services\Generator\Core\Evaluation;

class Result
{
    public string $baseClassName;
    public string $filePath;
    public string $namespace;
    public string $directory;
}