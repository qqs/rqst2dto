<?php

namespace Qqs\Rqst2Dto\Services\Generator;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;
use Qqs\Rqst2Dto\Services\Generator\Core\Evaluation\Evaluator;
use Qqs\Rqst2Dto\Services\Generator\Core\RulesConverter;
use Qqs\Rqst2Dto\Services\Generator\Core\TemplateGenerator;

class Generator
{
    private RulesConverter $rulesConverter;
    private TemplateGenerator $templateGenerator;
    private Evaluator $evaluator;

    public function __construct()
    {
        $this->rulesConverter = new RulesConverter();
        $this->templateGenerator = new TemplateGenerator();
        $this->evaluator = new Evaluator();
    }

    /**
     * Generate DTO file content from FormRequest.
     *
     * @param FormRequest $formRequest
     * @return string
     */
    public function generate(FormRequest $formRequest): bool
    {
        $properties = $this->rulesConverter->convert($formRequest->rules());
        $evaluation = $this->evaluator->evaluate($formRequest);
        $content = $this->templateGenerator->generate($evaluation->baseClassName, $evaluation->namespace, $properties);

        if (! File::isDirectory($evaluation->directory)) {
            File::makeDirectory($evaluation->directory, 0755, true);
        }
        return File::put($evaluation->filePath, $content);
    }

}