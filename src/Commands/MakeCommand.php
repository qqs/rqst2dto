<?php

namespace Qqs\Rqst2Dto\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Http\FormRequest;
use Qqs\Rqst2Dto\Services\Generator\Generator;

class MakeCommand extends Command
{
    protected $description = 'Create a new dto class from request class';

    protected $signature = 'make:dto {name?} {--all}';

    public function handle()
    {
        $name = $this->argument('name');
        $all = $this->option('all');
        $requestClasses = $this->getRequestClasses();

        if (! $name && ! $all) {
            $this->error('You must specify a request class name or use the --all flag.');
            return;
        }

        if ($all) {
            foreach ($requestClasses as $requestClass) {
                $this->makeDto($requestClass);
            }
        } else {
            $filteredClasses = array_filter($requestClasses, function ($class) use ($name) {
                return str_contains($class, $name);
            });
            if (empty($filteredClasses)) {
                $message = "Request class $name does not exist.";
                $this->info($message);
            }
            foreach ($filteredClasses as $requestClass) {
                $this->makeDto($requestClass);
            }
        }
    }

    protected function getRequestClasses()
    {
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(app_path())
        );

        $requestClasses = [];

        foreach ($files as $file) {
            if ($file->isFile()) {
                $namespace = 'App\\';
                $extensionCharsCount = 4;
                $slashCount = 1;
                $relativePath = substr($file->getPathname(), strlen(base_path('app')) + $slashCount, -$extensionCharsCount);
                $className = str_replace('/', '\\', $relativePath);
                $class = $namespace . $className;
//                dd($class);
//                dd($class, FormRequest::class, is_subclass_of($class, FormRequest::class));
                if (is_subclass_of($class, FormRequest::class)) {
//                    dd('ok');
                    $requestClasses[] = $class;
                }
            }
        }

        return $requestClasses;
    }

    protected function makeDto($requestClass)
    {

        $result = (new Generator)->generate(new $requestClass());

        if ($result !== false) {
            $this->info("DTO class $requestClass has been created.");
        }
    }
}