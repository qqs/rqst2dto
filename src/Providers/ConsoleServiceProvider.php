<?php

namespace Qqs\Rqst2Dto\Providers;

use Illuminate\Support\ServiceProvider;
use Qqs\Rqst2Dto\Commands\MakeCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                MakeCommand::class
            ]);
        }
    }

}